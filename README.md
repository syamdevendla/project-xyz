# Project XYZ

A RESTful web project to aggregate provided services: shipment, track, and pricing.

## Workspace

This project is tested and developed in a workspace using the following:

* ElementaryOS, Ubuntu-based Linux distribution
* OpenJDK 1.8.0_222
* Docker Community Edition 19.03.4
  
## Build & Run

### Project XYZ: Aggregate Service

The project takes three simple steps to build and run successfully. After cloning the project, run the following with the command terminal of your choice:

```shell
cd <project root directory>
./gradlew clean build
java -jar build/libs/project-xyz-*.jar
```

Health check is also simple using Spring Actuators. Go to the link: <http://localhost:8090/actuator/health>.

A healthy state should look like:

```json
{"status":"UP"}
```

> NOTE Use *./gradlew* to maintain consistent Gradle results

### xyzassessment/backend-services

Project XYZ depends on these backend-services. To run the backend-services, execute the following in a command terminal:

```shell
docker pull xyzassessment/backend-services
docker run -p 8080:8080 xyzassessment/backend-services
```

> NOTE The parameter *-p* guarantees port forwarding between the default and Docker container network space.

#### Stop Docker Service

Most running processes can be stopped via ```CTRL+C```. If the process is running in the background, execute the following:

```shell
docker stop xyzassessment/backend-services
```
