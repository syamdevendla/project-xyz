package com.kirkchan.poc.xyz.api.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class TrackQueue extends AbstractQueue {

    @Autowired
    public TrackQueue(RestTemplate restCaller) {
        super(restCaller, "http://localhost:8080/track?q=");
    }
}