package com.kirkchan.poc.xyz.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for failing to meeting input validation
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = 7689091921551045833L;

    public BadRequestException(String message) {
        super(message);
    }
}