package com.kirkchan.poc.xyz.api.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PricingQueue extends AbstractQueue {

    @Autowired
    public PricingQueue(RestTemplate restCaller) {
        super(restCaller, "http://localhost:8080/pricing?q=");
    }
}