package com.kirkchan.poc.xyz.api.queue;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

@EnableScheduling
public abstract class AbstractQueue {
    
    protected final Logger log = LoggerFactory.getLogger(getClass());

    // Time allowed for requests to live in queue
    public final long MAX_MS_QUEUE_LIFE = 5000;

    protected BlockingQueue<String[]> queue = new ArrayBlockingQueue<>(5);
    protected Map<String, ResponseEntity<JsonMapper>> responses = new ConcurrentHashMap<>();
    protected long oldest = 0;

    private RestTemplate restCaller;
    private String hostUrl;

    public AbstractQueue(RestTemplate restCaller, String hostUrl) {
        this.restCaller = restCaller;
        this.hostUrl = hostUrl;
    }

    /**
     * Force processing of requests if the oldest/first request reaches
     * max queue life.
     */
    @Scheduled(fixedRate = 1500) // 1500ms
    private void checkForOldRequests() {
        
        log.debug(String.format("Checking against oldest[%s], queue[%s]", this.oldest, this.queue.size()));

        if (this.oldest > 0 && this.queue.size() > 0) {
            long elapsed = System.currentTimeMillis() - this.oldest;

            log.debug(String.format("Calculated %s elapsed seconds...", elapsed));

            if (elapsed >= this.MAX_MS_QUEUE_LIFE) {
                
                log.info("Queue life exceed! Push requests.");
                this.oldest = 0;
                this.runQueue();
            }
        }
    }

    /**
     * Queue request and return the key of response when available
     * 
     * @param request
     * @return
     */
    public ResponseEntity<JsonMapper> queue(final String request) {

        // TODO Chances of the same key?
        String key = Integer.toString(new Random().nextInt(100000));
        final String[] req = { key, request };
        
        try {
            this.queue.offer(req, 150, TimeUnit.MILLISECONDS);

            if (this.queue.size() == 1) {
                this.oldest = System.currentTimeMillis();
                log.info(String.format("Oldest request queued at %s",
                    LocalDateTime.ofInstant(Instant.ofEpochMilli(this.oldest),
                    ZoneId.systemDefault())));
            }

            log.info(String.format("Queued request: %s; remaining capacity: [%s]", req[1], queue.remainingCapacity()));

            if (queue.remainingCapacity() == 0) {
                this.runQueue();
            }
        } catch (InterruptedException e) {
            log.info(String.format("Queue request [%s] interrupted", request));
        }

        JsonMapper acceptedResponse = new JsonMapper();
        acceptedResponse.setValue("status", "PROCESSING");
        acceptedResponse.setValue("key", key);

        return new ResponseEntity<JsonMapper>(acceptedResponse, HttpStatus.ACCEPTED);
    }

    protected void runQueue() {
        new Thread(new Consumer(restCaller, hostUrl, queue, this.responses)).start();
    }

    /**
     * Ideally, response is available if response is OK or SERVICE_UNAVAILABLE. ACCEPTED is 
     * the starting and processing state. Any change signifies a new response is available.
     * 
     * @param key
     * @return
     */
    public boolean isResponseAvailable(String key) {
        if (this.responses.get(key) == null)
            return false;

        return this.responses.get(key).getStatusCode().value() != HttpStatus.ACCEPTED.value();
    }

    /**
     * Return response object, if available in queue. Remove from queue after successful
     * response is returned. Otherwise, queue will hold the request until it expires or 
     * until memory is cleared.
     */
    public ResponseEntity<JsonMapper> pullResponse(String key) {
        ResponseEntity<JsonMapper> resp = this.responses.get(key);

        log.debug(String.format("Fetching key [%s] from stored responses %s", key, responses.entrySet().toString()));

        /**
         * This is a rare edge case if the key does not exist or before 
         * the request has completed the processing.
         */
        if (resp == null) {
            log.error(String.format("Key [%s] no longer available.", key));
            return new ResponseEntity<JsonMapper>(HttpStatus.NOT_FOUND);
        }

        if (resp.getStatusCode().value() == HttpStatus.OK.value()) {
            this.responses.remove(key);
        }

        return resp;
    }
}