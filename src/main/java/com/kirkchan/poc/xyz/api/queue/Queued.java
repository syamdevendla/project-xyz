package com.kirkchan.poc.xyz.api.queue;

/**
 * Object to help manage the different queued requests
 */
public class Queued {
    String shipmentKey;
    String trackKey;
    String pricingKey;

    public String getShipmentKey() {
        return shipmentKey;
    }

    public void setShipmentKey(String shipmentKey) {
        this.shipmentKey = shipmentKey;
    }

    public String getTrackKey() {
        return trackKey;
    }

    public void setTrackKey(String trackKey) {
        this.trackKey = trackKey;
    }

    public String getPricingKey() {
        return pricingKey;
    }

    public void setPricingKey(String pricingKey) {
        this.pricingKey = pricingKey;
    }
}