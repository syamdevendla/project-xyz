package com.kirkchan.poc.xyz.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for failing to meeting input validation
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = -8987611785932790148L;

    public NotFoundException(String message) {
        super(message);
    }
}