package com.kirkchan.poc.xyz.api;

import java.util.HashSet;
import java.util.Set;

import com.kirkchan.poc.xyz.api.exceptions.BadRequestException;
import com.kirkchan.poc.xyz.api.queue.Aggregate;
import com.kirkchan.poc.xyz.api.queue.QueueService;
import com.kirkchan.poc.xyz.api.queue.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

    private static final Logger log = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    QueueService queueService;

    /**
     * Looking to require all parameters so that queues can maintain same sizes
     * 
     * @param shipments
     * @param track
     * @param pricing
     * @return
     */
    @GetMapping("/aggregation")
    public ResponseEntity<Status> queue(@RequestParam String shipments, 
            @RequestParam String track, 
            @RequestParam String pricing) {

        this.validateQueries(shipments, track, pricing);
        
        return this.queueService.queueCalls(shipments, track, pricing);
    }

    @GetMapping("/aggregation/queue/{key}")
    public ResponseEntity<Status> checkQueue(@PathVariable String key) {

        this.validateKey(key);
        return this.queueService.checkQueue(key);
    }

    @GetMapping("/aggregation/{key}") 
    public ResponseEntity<Aggregate> fetchResponse(@PathVariable String key) {

        this.validateKey(key);
        return this.queueService.fetchAggregate(key);
    }

    /**
     * Keys are random numbers generated between 1-100,000
     * 
     * @param key
     */
    private void validateKey(String key) {
        boolean isValid = key.matches("[0-9]+");

        if (!isValid) {
            throw new BadRequestException("BAD REQUEST: Verify numeric key");
        }
    }

    private void validateQueries(String shipments, String track, String pricing) {
        
        Set<String> errors = new HashSet<>();

        if (!shipments.isEmpty()) {
            String[] shipmentOrders = shipments.split(",");

            for (String order : shipmentOrders) {
                if (!isValidOrder(order)) {
                    errors.add("Bad 9-digit order number");
                    log.info(String.format("Bad shipment request [%s]", shipments));
                    break;
                }
            }
        }

        if (!track.isEmpty()) {
            String[] trackOrders = track.split(",");

            for (String order : trackOrders) {
                if (!isValidOrder(order)) {
                    errors.add("9-digit order number");
                    log.info(String.format("Bad track request [%s]", track));
                    break;
                }
            }
        }

        if (!pricing.isEmpty()) {
            String[] isoCountries = pricing.split(",");

            for (String iso : isoCountries) {
                if (!isValidIso(iso)) {
                    errors.add("ISO-2 country code");
                    log.info(String.format("Bad pricing request [%s]", pricing));
                    break;
                }
            }
        }

        if(errors.size() > 0) {
            throw new BadRequestException("BAD REQUEST: Require " + errors.toString());
        }
    }

    // Orders are 9-digit numbers
    private boolean isValidOrder(String order) {
        return order.matches("[0-9]{9}");
    }

    // ISO-2 country codes
    private boolean isValidIso(String code) {
        return code.matches("[A-Z]{2}");
    }
}