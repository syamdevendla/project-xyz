package com.kirkchan.poc.xyz.api.queue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import com.kirkchan.poc.xyz.api.exceptions.SystemException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

public class Consumer implements Runnable {
    
    private static final Logger log = LoggerFactory.getLogger(Consumer.class);

    private BlockingQueue<String[]> queue;
    private Map<String, ResponseEntity<JsonMapper>> responses;

    private RestTemplate restCaller;
    private String hostUrl;

    public Consumer(RestTemplate restCaller, String hostUrl, 
            BlockingQueue<String[]> queue, Map<String, 
            ResponseEntity<JsonMapper>> responses) {
        this.restCaller = restCaller;
        this.hostUrl = hostUrl;
        this.queue = queue;
        this.responses = responses;
    }

    @Override
    public void run() {
        List<String[]> requests = new ArrayList<>();

        queue.drainTo(requests);
        log.info(String.format("Processing requests"));
        process(requests);
    }

    /**
     * Execute cluster of 5 API requets and store the results in the Map based on the request key.
     * 
     * @param requests
     */
    private void process(List<String[]> requests) {

        Map<String, List<String>> requestMap = new HashMap<>();
        String bigRequest = new String();

        // Collective keys to update if an error raises
        List<String> keys = new ArrayList<>();

        // Maintain original requests while building unified request
        for (String[] req : requests) {
            String key = req[0];

            keys.add(key);
            bigRequest += req[1] + ",";

            List<String> temp = Arrays.asList(req[1].split(","));
            requestMap.put(key, temp);
        }

        String url = this.hostUrl + bigRequest;
        // Don't need extra , at end
        if (bigRequest.charAt(bigRequest.length()-1) == ',')
            url = this.hostUrl + bigRequest.substring(0, bigRequest.length()-1);

        try {
            final ResponseEntity<JsonMapper> response = restCaller.getForEntity(url, JsonMapper.class);
            
            log.info(String.format("Processed aggregated backend request: [%s]", url));

            Map<String, Object> results = response.getBody().getValues();
            log.debug(String.format("Results %s", results.toString()));
            for (Map.Entry<String, List<String>> e : requestMap.entrySet()) {
                String key = e.getKey();
                
                // Map responses back to the original requests
                Map<String, Object> aResponseValues = new LinkedHashMap<>();
                for (String id : e.getValue()) {
                    aResponseValues.put(id, results.get(id));
                    log.debug(String.format("Mapping id[%s] to val[%s]", id, results.get(id)));
                }

                JsonMapper aResponse = new JsonMapper();
                aResponse.setValues(aResponseValues);
                responses.put(key, new ResponseEntity<JsonMapper>(aResponse, HttpStatus.OK));
                log.debug(String.format("Stored key [%s] with value [%s]", key, aResponseValues.toString()));
            }
        }
        catch (HttpServerErrorException e) {
            if (e.getRawStatusCode() == HttpStatus.SERVICE_UNAVAILABLE.value()) {
                log.info(String.format("Service unavailable while handling request %s",
                        url));

                for (String k : keys) {
                    responses.put(k, new ResponseEntity<JsonMapper>(new JsonMapper(), HttpStatus.SERVICE_UNAVAILABLE));
                }
            }
            else {
                log.error(String.format("Unexpected server error [%s] while handling request %s",
                        e.getRawStatusCode(), url));
                for (String k : keys) {
                    responses.put(k, new ResponseEntity<JsonMapper>(new JsonMapper(), HttpStatus.valueOf(e.getRawStatusCode())));
                }
            }
        }
        catch (Exception e) {
            log.error("Backend services is unavailable!", e);

            for (String k : keys) {
                responses.put(k, new ResponseEntity<JsonMapper>(new JsonMapper(), HttpStatus.SERVICE_UNAVAILABLE));
            }
            
            throw new SystemException("Backend services is unavailable; contact support tech.");
        }
    }
}