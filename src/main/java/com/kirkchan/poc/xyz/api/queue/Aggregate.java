package com.kirkchan.poc.xyz.api.queue;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonGetter;

public class Aggregate {

    // Order objects like examples:$
    private Map<String, Object> pricing;
    private Map<String, Object> track;
    private Map<String, Object> shipments;

    public Aggregate() {
        this.shipments = new HashMap<>();
        this.track = new HashMap<>();
        this.pricing = new HashMap<>();
    }

    public Aggregate(JsonMapper shipments, JsonMapper track, JsonMapper pricing) {
        
        // Forced null check; test suite creates null params when JsonMapper instance is empty
        this.shipments = shipments.getValues();
        this.track = track.getValues();
        this.pricing = pricing.getValues();
    }

    @JsonGetter
    public Map<String, Object> getPricing() {
        return pricing.isEmpty() ? null : pricing;
    }

    public void setPricing(Map<String, Object> pricing) {
        this.pricing = pricing;
    }

    @JsonGetter
    public Map<String, Object> getTrack() {
        return track.isEmpty() ? null : track;
    }

    public void setTrack(Map<String, Object> track) {
        this.track = track;
    }

    @JsonGetter
    public Map<String, Object> getShipments() {
        return shipments.isEmpty() ? null : shipments;
    }

    public void setShipments(Map<String, Object> shipments) {
        this.shipments = shipments;
    }

}