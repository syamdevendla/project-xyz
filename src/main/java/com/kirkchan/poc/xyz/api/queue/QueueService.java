package com.kirkchan.poc.xyz.api.queue;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.kirkchan.poc.xyz.api.exceptions.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * Main point of interaction; manages the entities and calls to the three different queues
 */
@Component
public class QueueService {

    @Autowired
    ShipmentQueue shipmentQueue;

    @Autowired
    TrackQueue trackQueue;

    @Autowired
    PricingQueue pricingQueue;

    // Aggregate of queues
    private Map<String, Queued> queues = new HashMap<>();

    /**
     * Queue shipments, track, and pricing requests
     * 
     * @param shipments
     * @param track
     * @param pricing
     * @return
     */
    public ResponseEntity<Status> queueCalls(String shipment, String track, String pricing) {

        ResponseEntity<JsonMapper> shipmentResponse = this.shipmentQueue.queue(shipment);
        ResponseEntity<JsonMapper> trackResponse = this.trackQueue.queue(track);
        ResponseEntity<JsonMapper> pricingResponse = this.pricingQueue.queue(pricing);

        String key = Integer.toString(new Random().nextInt(100000));

        String shipmentKey = (String) shipmentResponse.getBody().getValues().get("key");
        String trackKey = (String) trackResponse.getBody().getValues().get("key");
        String pricingKey = (String) pricingResponse.getBody().getValues().get("key");

        Queued queued = new Queued();
        queued.setShipmentKey(shipmentKey);
        queued.setTrackKey(trackKey);
        queued.setPricingKey(pricingKey);
        queues.put(key, queued);

        Status acceptedResponse = new Status();
        acceptedResponse.setStatus("PROCESSING");
        acceptedResponse.setLocation("/aggregation/queue/" + key);
        
        return new ResponseEntity<Status>(acceptedResponse, HttpStatus.ACCEPTED);
    }

    /**
     * Check queued request for shipment, track, and pricing based on the key given to the original
     * order. If all responses are available, a new resource will be available and provided in 
     * response.
     * 
     * @param key
     * @return
     */
    public ResponseEntity<Status> checkQueue(String key) {

        boolean available = this.isAvailableFor(key);
        Status queueResponse = new Status();

        if (available) {

            queueResponse.setStatus("READY");
            queueResponse.setLocation("/aggregation/" + key);

            return new ResponseEntity<Status>(queueResponse, HttpStatus.SEE_OTHER);
        }
        else {
            
            queueResponse.setStatus("PROCESSING");
            queueResponse.setLocation("/aggregation/queue/" + key);
            
            return new ResponseEntity<Status>(queueResponse, HttpStatus.ACCEPTED);
        }
    }

    public ResponseEntity<Aggregate> fetchAggregate(String key) {
        
        boolean available = this.isAvailableFor(key);

        Queued keyset = this.queues.get(key);
        if (available) {
            JsonMapper shipment = this.shipmentQueue.pullResponse(keyset.getShipmentKey()).getBody();
            JsonMapper track = this.trackQueue.pullResponse(keyset.getTrackKey()).getBody();
            JsonMapper pricing = this.pricingQueue.pullResponse(keyset.getPricingKey()).getBody();

            Aggregate aggregate = new Aggregate(shipment, track, pricing);
            return new ResponseEntity<Aggregate>(aggregate, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Aggregate>(new Aggregate(), HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    // Condition when to return response
    private boolean isAvailableFor(String key) {

        Queued queued = this.queues.get(key);

        if (queued == null) {
            throw new NotFoundException(String.format("Key [%s] is a bad key or not found.", key));
        }

        boolean shipmentAvailable = this.shipmentQueue.isResponseAvailable(queued.getShipmentKey());
        boolean trackAvailable = this.trackQueue.isResponseAvailable(queued.getTrackKey());
        boolean pricingAvailable = this.pricingQueue.isResponseAvailable(queued.getPricingKey());

        return shipmentAvailable && trackAvailable && pricingAvailable;
    }
}